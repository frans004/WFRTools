# How to install
Start R

Load the library called devtools
```R
library(devtools)
```
Install WFRTools:
```R
install_git("https://git.wur.nl/frans004/WFRTools.git")
```
